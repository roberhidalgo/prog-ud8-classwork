package actividad9;

import java.util.HashMap;
import java.util.Map;

public class Diccionario {

    private HashMap<String, String> entradas;

    public Diccionario() {

        this.entradas = new HashMap<>();
    }

    public void cargarContenido(){
        this.entradas.put("hola", "hello");
        this.entradas.put("adiós", "goobbye");
        this.entradas.put("ratón", "mouse");
        this.entradas.put("teclado", "keyboard");
        this.entradas.put("ordenador", "computer");
        this.entradas.put("red", "network");
    }

    public String getTraduccion(String espanyol) {
        return this.entradas.get(espanyol);
    }

}
