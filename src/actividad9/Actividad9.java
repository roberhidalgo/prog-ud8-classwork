package actividad9;

import java.util.Scanner;

public class Actividad9 {

    public static void main(String[] args) {

        Diccionario diccionario = new Diccionario();
        diccionario.cargarContenido();

        Scanner scanner = new Scanner(System.in);
        System.out.print("Dime una palabra en español: ");
        String palabraEspanyol = scanner.next();

        String traduccion = diccionario.getTraduccion(palabraEspanyol);
        if (traduccion != null) {
            System.out.println(palabraEspanyol + " en inglés se dice " + traduccion);
        } else {
            System.out.println("No conozco esa palabra");
        }
    }

}
