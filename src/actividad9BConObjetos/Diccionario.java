package actividad9BConObjetos;

import java.util.HashMap;
import java.util.Random;

public class Diccionario {

    private HashMap<String, String> entradas;

    public Diccionario() {

        this.entradas = new HashMap<>();
        cargarContenido();
    }

    private void cargarContenido(){
        this.entradas.put("hola", "hello");
        this.entradas.put("adiós", "goobbye");
        this.entradas.put("ratón", "mouse");
        this.entradas.put("teclado", "keyboard");
        this.entradas.put("ordenador", "computer");
        this.entradas.put("red", "network");
    }

    public String getPalabraAleatoria() {
        String[] palabrasEspanyol = this.entradas.keySet().toArray(new String[0]);
        Random random = new Random();
        return palabrasEspanyol[random.nextInt(palabrasEspanyol.length)];

    }
    public boolean esTraduccionCorrecta(String palabraEspanyol, String palabraIngles) {
        String traduccionIngles = this.entradas.get(palabraEspanyol);
        if (traduccionIngles != null)
            return traduccionIngles.equalsIgnoreCase(palabraIngles);
        else
            return false;
    }

}
