package actividad9BConObjetos;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class JuegoDiccionario {

    private static final int NUM_PALABRAS_SELECCIONADAS = 3;
    private Diccionario diccionario;
    private HashMap<String, Boolean> palabrasSeleccionadas;

    public JuegoDiccionario() {
        this.diccionario = new Diccionario();
        seleccionarPalabras();
    }

    private void seleccionarPalabras() {

        this.palabrasSeleccionadas = new HashMap<>();

        for (int i = 0; i < NUM_PALABRAS_SELECCIONADAS; i++) {
            String palabra = this.diccionario.getPalabraAleatoria();

            if (!this.palabrasSeleccionadas.containsKey(palabra)) {
                this.palabrasSeleccionadas.put(palabra, null);
            } else {
                i--;
            }
        }
    }

    public void jugar() {
        iniciarJuego();
        mostrarResultados();
    }

    private void iniciarJuego() {
        Scanner scanner = new Scanner(System.in);
        for (Map.Entry palabraSeleccionada: palabrasSeleccionadas.entrySet()) {
            String palabraEspanyol = (String) palabraSeleccionada.getKey();
            System.out.println("¿Cómo se dice " + palabraEspanyol + " en inglés?");

            String respuesta = scanner.next();
            boolean seHaAcertadoLaTraduccion = this.diccionario.esTraduccionCorrecta(palabraEspanyol, respuesta);
            this.palabrasSeleccionadas.replace(palabraEspanyol, seHaAcertadoLaTraduccion);
        }
    }

    private void mostrarResultados() {
        int correctas = 0, incorrectas = 0;

        for (Map.Entry palabraSeleccionada: palabrasSeleccionadas.entrySet()) {
            if ((boolean)palabraSeleccionada.getValue()) {
                correctas++;
            } else {
                incorrectas++;
            }
        }

        System.out.println("Correctas: " + correctas);
        System.out.println("Incorrectas: " + incorrectas);
    }


}
