package actividad7;

import java.util.ArrayList;
import java.util.HashSet;

public class Actividad7 {

    public static void main(String[] args) {
        ArrayList<Participante>prueba1 = new ArrayList<>();
        prueba1.add(new Participante("64112243L","Toni García",  10.12f));
        prueba1.add(new Participante("63781231A", "Elena Compte",  12.23f));
        prueba1.add(new Participante("42342352B", "María Pérez",  15.30f));
        prueba1.add(new Participante("453452342A", "Juan Pérez", 18.30f));

        ArrayList<Participante>prueba2 = new ArrayList<>();
        prueba2.add(new Participante("64112243L","Toni García",  10.12f));
        prueba2.add(new Participante("78675464E", "Ernesto Melloso",  12.23f));
        prueba2.add(new Participante("44124124Q", "Toni Nada",  15.30f));
        prueba2.add(new Participante("12312312A", "Carmen Tira", 18.30f));

        HashSet<Participante> participantes = new HashSet<>();
        participantes.addAll(prueba1);
        participantes.addAll(prueba2);

        for (Participante participante: participantes) {
            System.out.println(participante);
        }
    }
}
