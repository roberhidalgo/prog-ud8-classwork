package actividad10;

import java.util.Scanner;

public class TestLoginService {

    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        TestLoginService testLoginService = new TestLoginService();
        testLoginService.iniciar();
    }

    private void iniciar() {

        LoginService loginService = new LoginService();

        for (int i = 0; i < LoginService.MAX_INTENTOS; i++) {
            if (loginService.login(getUserName(), getPassword())) {
                System.out.println("Usuario logueado Correctamente");
                return;
            } else {
                System.out.println("El usuario o password introducido es incorrecto. Te quedan " + (LoginService.MAX_INTENTOS - i - 1) + " intentos para loguearte.");
            }
        }

        // Si llega a este punto, el login estará bloqueado.
        loginService.login(getUserName(), getPassword());
    }

    private static String getUserName(){
        System.out.print("Introduce el nombre de usuario: ");
        return input.next();
    }

    private static String getPassword(){
        System.out.print("Introduce el password: ");
        return input.next();
    }
}
