package actividad9BSinObjetos;

import java.util.*;

public class Actividad9b {

    public static void main(String[] args) {

        HashMap<String, String> diccionario = Diccionario.crearDiccionario();
        HashMap<String, Boolean> resultadoJuego = JuegoDiccionario.jugar(diccionario);
        JuegoDiccionario.mostrarResultados(resultadoJuego);
    }
}
