package actividad9BSinObjetos;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class JuegoDiccionario {

    private static final int NUM_PALABRAS_SELECCIONADAS = 3;

    public static HashMap<String, Boolean> jugar(HashMap<String, String> diccionario) {

        HashMap<String, Boolean> palabrasSeleccionadas = new HashMap<>();
        seleccionarPalabras(diccionario, palabrasSeleccionadas);

        Scanner scanner = new Scanner(System.in);

        for (Map.Entry palabraSeleccionada : palabrasSeleccionadas.entrySet()) {

            String palabraEspanyol = (String) palabraSeleccionada.getKey();

            System.out.println("¿Cómo se dice " + palabraEspanyol + " en inglés?");
            boolean resultadoTraduccion = Diccionario.esTraduccionCorrecta(diccionario, palabraEspanyol, scanner.next());
            palabrasSeleccionadas.replace(palabraEspanyol, resultadoTraduccion);
        }

        return palabrasSeleccionadas;
    }

    private static void seleccionarPalabras(HashMap<String, String> diccionario, HashMap<String, Boolean> palabrasSeleccionadas) {

        for (int i = 0; i < NUM_PALABRAS_SELECCIONADAS; i++) {
            String palabra = Diccionario.getPalabraAleatoria(diccionario);

            if (!palabrasSeleccionadas.containsKey(palabra)) {
                palabrasSeleccionadas.put(palabra, null);
            } else {
                i--;
            }
        }
    }

    public static void mostrarResultados(HashMap<String, Boolean> palabrasSeleccionadas) {
        int correctas = 0, incorrectas = 0;

        for (Map.Entry palabraSeleccionada: palabrasSeleccionadas.entrySet()) {
            if ((boolean)palabraSeleccionada.getValue()) {
                correctas++;
            } else {
                incorrectas++;
            }
        }

        System.out.println("Correctas: " + correctas);
        System.out.println("Incorrectas: " + incorrectas);
    }
}
