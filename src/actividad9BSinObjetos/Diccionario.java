package actividad9BSinObjetos;

import java.util.HashMap;
import java.util.Random;

public class Diccionario {

    public static HashMap<String, String> crearDiccionario() {
        HashMap<String, String> miniDiccionario = new HashMap<>();
        miniDiccionario.put("hola", "hello");
        miniDiccionario.put("adiós", "bye");
        miniDiccionario.put("ordenador", "computer");
        miniDiccionario.put("objeto", "object");
        miniDiccionario.put("lenguaje", "language");
        miniDiccionario.put("langosta", "lobster");
        miniDiccionario.put("azul", "blue");
        miniDiccionario.put("lápiz", "pencil");
        return miniDiccionario;
    }

    public static String getPalabraAleatoria(HashMap<String, String> diccionario) {
        String palabrasEspanyol[] = diccionario.keySet().toArray(new String[0]);
        Random random = new Random();
        return palabrasEspanyol[random.nextInt(palabrasEspanyol.length)];
    }

    public static boolean esTraduccionCorrecta(HashMap<String, String> diccionario, String palabraEspanyol, String palabraIngles) {
        String traduccionIngles = diccionario.get(palabraEspanyol);
        if (traduccionIngles != null)
            return traduccionIngles.equalsIgnoreCase(palabraIngles);
        else
            return false;
    }
}
