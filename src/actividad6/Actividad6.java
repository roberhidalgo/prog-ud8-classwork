package actividad6;

import java.util.HashSet;
import java.util.Scanner;

public class Actividad6 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String texto;
        HashSet<String>nombres = new HashSet<>();
        do {
            texto = scanner.next();

            if (!texto.equalsIgnoreCase("Fin"))
                nombres.add(texto);

        }while(!texto.equalsIgnoreCase("Fin"));

        for (String nombre: nombres) {
            System.out.println(nombre);
        }
    }

}
